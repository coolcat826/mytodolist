import React, { useState, useEffect } from "react";
import { Card, Table, Button, Form } from "react-bootstrap";
import ReactDOM from "react-dom";
import { convertToObject, getDefaultLibFileName } from "typescript";
import { createRoot } from 'react-dom/client';
import Addtodo from "./Addtodo";
import App from "./App";
//項目清單
interface myParam {
    mytodo: myTodo[];
}

export const Listitem: React.FC<myParam> = ({ mytodo }) => {
    const [pk, setpk] = useState('');//
    const [editevent, seteditevent] = useState('');//新增項目
    const [editpriority, seteditpriority] = useState('');//優先權
    const [select_priority,setselect_priority]=useState('');//查詢優先權
    const [smsg, setMsg] = useState('');
    const [items, setItems] = useState([]);
    const [filter_mytodo,setfilter_mytodo]=useState("");
    const url = "http://localhost:5001/todolist/";
    const [mykeyword, setmykeyword] = useState('');//關鍵字搜尋
    const [keywordtodo,setkeywordtodo] = useState('');
    
    console.log(editevent);
    console.log(mytodo);

    //項目更新
    function change_event(event: any, pk: string) {
        console.log("edit");
        setTimeout(function () {
            seteditevent(event.target.value);
            setpk(pk);
        }, 1000);

        if (event.keyCode === 13) {
            if (editevent === "") { return }
            console.log("13" + editevent);

            const requestOptions = {
                method: 'PATCH',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({ myevent: editevent, pk: pk })
            };
            fetch(url, requestOptions)
                .then(response => response.json())
                .then(json => setMsg(json.statecode.smsg)
                );
            alert("已更新")
            window.location.reload();

        }
    }
    //改變優先權
    function change_search_priority(event:any){
        console.log("setselect_priority", select_priority);
        
        if(select_priority == '0'){
            window.location.reload();
        }else{
            setselect_priority(event.target.value);
        }
        
        console.log("setselect_priority", select_priority);
    }

    //查詢優先權
    function usersearch_priority(select_priority:string) {
            setselect_priority(select_priority);
            if(select_priority=="0"){
                window.location.reload();
            }
            setTimeout(function () {

            //setselect_priority(usersearch_priority);
            console.log("setselect_priorityAA", select_priority);
            
            let filter_mytodo = mytodo.filter(mytodo => mytodo.priority == parseInt(select_priority));


            console.log("filter"+filter_mytodo);

            ReactDOM.render(
                  <Listitem mytodo={filter_mytodo} />,
                  document.getElementById('root')
            )
            
        }, 1000);
    }
    
    useEffect(() => {
        if (select_priority === ""){ 
            return
        }else{
            usersearch_priority(select_priority);
        };

    }, [select_priority]);


    //更新優先權
    const handleChangeSelect = (event: any, pk: string) => {
        seteditpriority(event.target.value);
        setpk(pk);
    };

    function fetchSpecification(editpriority: any) {
        setTimeout(function () {
            console.log("seteditpriority", editpriority);
            seteditpriority(editpriority);

            const requestOptions = {
                method: 'PATCH',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({ priority: editpriority, pk: pk })
            };
            fetch(url, requestOptions)
                .then(response => response.json())
                .then(json => setMsg(json.statecode.smsg)
                );
            alert("已更新")
            window.location.reload();

        }, 1000);
    }

    useEffect(() => {
        if (editpriority === ""){ 
            return
        }else{
            fetchSpecification(editpriority);
        };

    }, [editpriority]);

    //刪除
    function delitem(e: any, pk: string) {
        setpk(pk);
        const requestOptions = {
            method: 'PATCH',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ del: '1', pk: pk })
        };
        fetch(url, requestOptions)
            .then(response => response.json())
            .then(json => setMsg(json.statecode.smsg)
            );
        alert("已刪除")
        window.location.reload();
    }

    //查詢關鍵字
    function usersearch_keyword(event:any) {
        
        setTimeout(function () {
            setmykeyword(event.target.value);
            console.log("usersearch_keyword"+event.keyCode);
    
        }, 1000);
    }
    function change_search_keyword(mykeyword:string){

        let keywordtodo = mytodo.filter(mytodo =>mytodo.myevent.includes(mykeyword));

            console.log("filter"+keywordtodo);
            
            ReactDOM.render(
               
                  <Listitem mytodo={keywordtodo} />,
                  document.getElementById('root')
              
            )
    }

    useEffect(() => {
        if (mykeyword === ""){ 
            return
        }else{
            change_search_keyword(mykeyword);
        };

    }, [mykeyword]);

    function reflash(e:any){
        window.location.reload();
    }

    return (
        <>
            <Addtodo></Addtodo>
            <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', marginTop: '30px' }}>
                <Card style={{ width: '60%' }}>
                    <Table striped bordered hover>
                        <thead>
                            <tr>
                                <th>
                                    <Button onClick={(e) => reflash(e)}>※</Button>
                                </th>
                                <th style={{ width: "50%" }}>
                                    <Form.Control type="text" defaultValue={""} onKeyDown={(e) => { usersearch_keyword(e)}} placeholder="關鍵字搜尋......" />
                                </th>
                                <th><Form.Select onChange={(e) => {change_search_priority(e)}}>
                                            <option value={"0"}>優先權</option>
                                            <option value={"0"}>全部</option>
                                            <option value={"1"}>High</option>
                                            <option value={"2"}>medium</option>
                                            <option value={"3"}>low</option>
                                            <option value={"99"} >done</option>
                                        </Form.Select></th>
                                <th>更新日</th>
                                <th>刪除</th>
                            </tr>
                        </thead>
                        <tbody>
                            {mytodo.map((item, index) => (

                                <tr>
                                    <td>{index + 1}</td>
                                    <td style={{ textAlign: 'left' }}>
                                        <label
                                            style={{
                                                textDecoration: item.priority == 99 ? 'line-through' : undefined,
                                            }}
                                        >
                                            <Form.Control type="text" id="myitem" defaultValue={item.myevent} disabled={item.priority == 99 ? true : false} onKeyDown={(e) => { change_event(e, item.pk) }}></Form.Control>

                                        </label>
                                    </td>
                                    <td>
                                        <Form.Select defaultValue={item.priority} onChange={(e) => { handleChangeSelect(e, item.pk) }}>
                                            <option value={"1"}>High</option>
                                            <option value={"2"}>medium</option>
                                            <option value={"3"}>low</option>
                                            <option value={"99"} >done</option>
                                        </Form.Select>
                                    </td>
                                    <td>{item.update_time}</td>
                                    <td><Button onClick={(e) => delitem(e, item.pk)}>X</Button></td>
                                </tr>
                            ))}
                        </tbody>
                    </Table>
                    {/* </Card.Body> */}
                </Card>
            </div >
        </>
    );

   
};


