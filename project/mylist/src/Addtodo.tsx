import React, { useState, useEffect, useContext } from "react";
import { Card, Button, Form } from "react-bootstrap";
import ReactDOM from 'react-dom';
import { Listitem } from './listitem';
import App from './App';

function Addtodo() {
    const [addtext, setaddtext] = useState('');//新增項目
    const [addpriority, setaddpriority] = useState('3');//優先權
    const [error, setError] = useState(null);
    const [msg, setMsg] = useState([]);
  
    //新增項目
    function addItem(e: any) {
        console.log("hihi");
        console.log({ addtext });
        console.log({ addpriority });
        if (addtext === "") {
            alert("請輸入項目");
            return
        };

        const url = "http://localhost:5001/todolist/";

        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ myevent: addtext, priority: addpriority })
        };
        fetch(url, requestOptions)
            .then(response => response.json())
            .then(json => setMsg(json.statecode.smsg)
            );

        console.log(msg);
        alert("新增成功" + msg);
        e.preventDefault();
        setaddtext("");
        window.location.reload();
    }

    if (error) {
        return <div> Error:{error}</div>
    } else {
        return (
            <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', marginTop: '30px' }}>
                <Card style={{ width: '60%' }}>
                    <Card.Header>
                        <h4>TODOLIST</h4>
                    </Card.Header>
                    <Card.Body>
                        <Form.Control type="text" value={addtext} onChange={(e) => { setaddtext(e.target.value) }} placeholder="新增項目" />
                        優先權:
                        <Form.Select defaultValue={addpriority} onChange={(e) => { setaddpriority(e.target.value) }}>
                            <option value="1">High</option>
                            <option value="2">medium</option>
                            <option value="3">low</option>
                        </Form.Select>
                        <Button variant="success" onClick={(e) => { addItem(e) }} style={{ marginTop: '10px', marginBottom: '30px' }}>+ADD新增</Button>

                    </Card.Body>
                </Card>
            </div >
        );
    }
}

export default Addtodo;





